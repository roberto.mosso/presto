<?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.layout','data' => []]); ?>
<?php $component->withName('layout'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes([]); ?>

    <div class="container mt-5">
        <div class="row text-center">
            <div class="col-12 my-4">
                <h1 class="text-center h1-view">Cestino</h1>
            </div>
        </div>
    </div>

    <?php if($announcements): ?>
        <?php $__currentLoopData = $announcements; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $announcement): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="container my-5 vh-100">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                Annuncio # <?php echo e($announcement->id); ?>

                            </div>
                            <div class="card-body">
                                <div class="row my-3">
                                    <div class="col-md-3">
                                        <h3>Utente</h3>
                                    </div>
                                    <div class="col-md-9">
                                        # <?php echo e($announcement->user->id); ?>,
                                        <?php echo e($announcement->user->name); ?>,
                                        <?php echo e($announcement->user->email); ?>

                                    </div>
                                </div>
                                <div class="row my-3">
                                    <div class="col-md-3">
                                        <h3>Titolo</h3>
                                    </div>
                                    <div class="col-md-9">
                                        <?php echo e($announcement->title); ?>

                                    </div>
                                </div>
                                <div class="row my-3">
                                    <div class="col-md-3">
                                        <h3>Prezzo</h3>
                                    </div>
                                    <div class="col-md-9">
                                        <?php echo e($announcement->price); ?> €
                                    </div>
                                </div>
                                <div class="row my-3">
                                    <div class="col-md-3">
                                        <h3>Descrizione</h3>
                                    </div>
                                    <div class="col-md-9">
                                        <?php echo e($announcement->description); ?>

                                    </div>
                                </div>
                                <div class="row mt-5">
                                    <div class="col-md-3">
                                        <h3>Immagini</h3>
                                    </div>
                                    <div class="col-md-9">

                                        <?php $__currentLoopData = $announcement->images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                            <div class="row">

                                                

                                                <div class="col-md-5 mb-4">
                                                    <img src="<?php echo e($image->getUrl(320,320)); ?>" class="rounded" alt="">
                                                </div>

                                                <div class="col-md-3 mb-4">
                                                    Adult: <?php echo e($image->adult); ?> <br>
                                                    Spoof: <?php echo e($image->spoof); ?> <br>
                                                    Medical: <?php echo e($image->medical); ?> <br>
                                                    Violence: <?php echo e($image->violence); ?> <br>
                                                    Racy: <?php echo e($image->racy); ?> <br>
                                                </div>

                                                <div class="col-md-3 mb-4">
                                                    <h5>Labels</h5>
                                                    <ul>
                                                    <?php if($image->labels): ?>
                                                        <?php $__currentLoopData = $image->labels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $label): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <li><?php echo e($label); ?></li>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                    </ul>
                                                </div>

                                            </div>
                                            
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                      
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
                <div class="card-footer">
                    <div class="row my-3">
                        <div class="col-md-6 text-center">
                            <form action="<?php echo e(route('revisor.accetta', $announcement->id)); ?>" method="POST">
                                <?php echo csrf_field(); ?>
                                <button type="submit" class="btn btn-success py-2 px-5">Accetta</button>
                            </form>
                        </div>
                        <div class="col-md-6 text-center">
                            <form action="<?php echo e(route('revisor.destroy', $announcement->id)); ?>" method="POST">
                                <?php echo csrf_field(); ?>
                                <?php echo method_field('DELETE'); ?>
                                <button type="submit" class="btn btn-danger py-2 px-5">Cancella definitivamente</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php else: ?>
    <?php endif; ?>

    <?php if(\App\Models\Announcement::trashCount() == 0): ?>
        <div class="container mt-5 vh-100">
            <div class="row text-center">
                <div class="col-12 my-4">
                    <h2 class="text-center">Non ci sono annunci nel cestino</h2>
                </div>
            </div>
        </div>

    <?php endif; ?>

    <div class="row justify-content-center mt-3">
        <div class="col-md-8">
            <?php echo e($announcements->links()); ?>

        </div>
    </div>

 <?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?>
<?php /**PATH C:\Users\Roberto\wa\progetti\presto\resources\views/revisor/trash.blade.php ENDPATH**/ ?>