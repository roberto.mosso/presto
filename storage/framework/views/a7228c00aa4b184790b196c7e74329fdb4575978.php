<?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.layout','data' => []]); ?>
<?php $component->withName('layout'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes([]); ?>
    <div class="container">
        <div class="card my-5">
            <div class="row">
                <div class="col-md-5 my-auto">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <?php $__currentLoopData = $announcement->images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($loop->first): ?>
                                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                <?php else: ?>
                                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            
                        </ol>
                        
                        <div class="carousel-inner">
                            
                            <?php $__currentLoopData = $announcement->images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($loop->first): ?>
                                <div class="carousel-item active">
                                    <img src="<?php echo e($image->getUrl(320,320)); ?>" class="d-block w-100"  alt=""> 
                                </div>
                                <?php else: ?>
                                <div class="carousel-item">
                                    <img src="<?php echo e($image->getUrl(320,320)); ?>" class="d-block w-100"  alt=""> 
                                </div>
                                <?php endif; ?>
                                    
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           
                        </div>

                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                            data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>

                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                            data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>


                <div class="col-md-7">
                    <div class="card-body">
                        <p class="my-4">Categorie: <a
                                href="<?php echo e(route('public.announcementsByCategory', [$announcement->category->name, $announcement->category->id])); ?>"><?php echo e($announcement->category->name); ?></a>
                        </p>
                        <h4 class="card-title"><?php echo e($announcement->title); ?></h4>
                        <p class="card-text my-4">Prezzo: <?php echo e($announcement->price); ?> €</p>
                        <p class="card-text my-4"><?php echo e($announcement->description); ?></p>

                        <a href="<?php echo e(route('home')); ?>" class="btn btn-orange mt-3">Torna indietro</a>
                    </div>
                </div>


            </div>

            <div class="card-footer">
                <div class="col-12 col-md-6">
                    <span class="text-muted">Pubblicato da: <?php echo e($announcement->user->name); ?></span>
                </div>
                <div class="col-12 col-md-6">
                    <span class="text-muted">In data: <?php echo e($announcement->created_at->format('d-m-Y')); ?></span>
                </div>

            </div>
        </div>

    </div>


 <?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?>
<?php /**PATH C:\Users\Roberto\wa\progetti\presto\resources\views/announcements/detail.blade.php ENDPATH**/ ?>