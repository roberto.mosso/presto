<?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.layout','data' => []]); ?>
<?php $component->withName('layout'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes([]); ?>

    <div class="container">

        <div class="row my-5">
            <div class="col-12 text-center">
                <h1>Benvenuto <?php echo e($user->name); ?></h1>
            </div>
        </div>

        <div class="row my-5">

            <?php $__currentLoopData = $announcements; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $announcement): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                <?php if($announcement->user_id == $user->id): ?>
                    <div class="col-12 col-md-6 col-lg-4 d-flex justify-content-center my-4">
                        <div class="card mt-4" style="width: 18rem;">

                            <?php if($announcement->images->first() == null): ?>

                                <img src="https://via.placeholder.com/320x320" class="card-img-top" alt="salve">

                            <?php else: ?>

                                <img src="<?php echo e($announcement->images->first()->getUrl(320, 320)); ?>" class="card-img-top"
                                    alt="ciao">

                            <?php endif; ?>

                            <div class="card-body">
                                <p class="text-center"><?php echo e(__('ui.category')); ?>:
                                    <a
                                        href="<?php echo e(route('public.announcementsByCategory', [$announcement->category->name, $announcement->category->id])); ?>"><?php echo e($announcement->category->name); ?></a>
                                </p>

                                <h4 class="card-title my-4"><?php echo e($announcement->title); ?></h4>

                                <p class="card-text"><?php echo e(__('ui.price')); ?>: <?php echo e($announcement->price); ?> €</p>

                                <p class="card-text"><?php echo e($announcement->troncate($announcement->description)); ?></p>

                                <p class="card-text"><?php echo e(__('ui.author-data')); ?>: <?php echo e($announcement->user->name); ?>,
                                    <?php echo e($announcement->created_at->format('d-m-Y')); ?></p>
                                <div class="text-center">
                                    <a href="<?php echo e(route('announcements.detail', $announcement)); ?>"
                                        class="btn btn-orange my-3"><?php echo e(__('ui.go-announcement')); ?></a>
                                </div>

                                <div class="text-center">
                                    <a href="<?php echo e(route('announcement.edit', $announcement)); ?>"
                                        class="btn btn-orange my-3">Modifica</a>
                                </div>

                                

                            </div>
                        </div>
                    </div>
                <?php endif; ?>

            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </div>

    </div>

 <?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?>
<?php /**PATH C:\Users\Roberto\wa\progetti\presto\resources\views/announcements/userpage.blade.php ENDPATH**/ ?>