<?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.layout','data' => []]); ?>
<?php $component->withName('layout'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes([]); ?>

    <header class="masthead img-fluid">
        <div class="container h-100">
            <div class="row justify-content-center">
                <div class="col-6">
                    <?php if(session('success')): ?>
                        <div class="alert alert-success text-center mt-2">
                            <?php echo e(session('success')); ?>

                        </div>
                    <?php endif; ?>
                    <?php if(session('access.denied.revisor.only')): ?>
                        <div class="alert alert-danger text-center mt-2">
                            Accesso non consentito - solo per revisori
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row h-100 align-items-center">
                <div id="header-index"
                    class="col-12 col-md-6 text-center bg-filter d-flex flex-column justify-content-center align-items-center offset-md-6 p-2">
                    <h1 class="font-weight-bold title-custom h1-header mt-2">Presto</h1>
                    <p class="h4 text-header p-header"><?php echo e(__('ui.welcome')); ?></p>

                    <form action="<?php echo e(route('announcements.searchResults')); ?>" method="GET">
                        <div class="wrap mt-3 mb-4">
                            <div class="search w-auto">
                                <input type="text" name="q" class="searchTerm" placeholder="<?php echo e(__('ui.search')); ?>">
                                <button type="submit" class="searchButton">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </header>


    <div class="container mt-5">
        <div class="row text-center">
            <div class="col-12 my-4">
                <h1 class="text-center h1-view"><?php echo e(__('ui.last-announcements')); ?></h1>
            </div>
        </div>
        <div class="row">

            <?php $__currentLoopData = $announcements; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $announcement): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                <div class="col-12 col-md-6 col-lg-4 d-flex justify-content-center my-4">
                    <div class="card mt-4" style="width: 18rem;">

                        <?php if($announcement->images->first() == null): ?>

                            <img src="https://via.placeholder.com/320x320" class="card-img-top" alt="salve">

                        <?php else: ?>

                            <img src="<?php echo e($announcement->images->first()->getUrl(320, 320)); ?>" class="card-img-top"
                                alt="ciao">

                        <?php endif; ?>

                        <div class="card-body">
                            <p class="text-center"><?php echo e(__('ui.category')); ?>:
                                <a
                                    href="<?php echo e(route('public.announcementsByCategory', [$announcement->category->name, $announcement->category->id])); ?>"><?php echo e($announcement->category->name); ?></a>
                            </p>

                            <h4 class="card-title my-4"><?php echo e($announcement->title); ?></h4>

                            <p class="card-text"><?php echo e(__('ui.price')); ?>: <?php echo e($announcement->price); ?> €</p>

                            <p class="card-text"><?php echo e($announcement->troncate($announcement->description)); ?></p>

                            <p class="card-text"><?php echo e(__('ui.author-data')); ?>: <?php echo e($announcement->user->name); ?>,
                                <?php echo e($announcement->created_at->format('d-m-Y')); ?></p>
                            <div class="text-center">
                                <a href="<?php echo e(route('announcements.detail', $announcement)); ?>"
                                    class="btn btn-orange my-3"><?php echo e(__('ui.go-announcement')); ?></a>
                            </div>

                        </div>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </div>
    </div>


 <?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?>
<?php /**PATH C:\Users\Roberto\wa\progetti\presto\resources\views/home.blade.php ENDPATH**/ ?>