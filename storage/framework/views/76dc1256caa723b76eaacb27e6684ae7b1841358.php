<?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.layout','data' => []]); ?>
<?php $component->withName('layout'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes([]); ?>


    <div class="container-fluid" id="body">

        <div class="row">
            <div class="col-12">
                <h1 class="titolo-form text-center text-blue mt-5 mb-3 h1-view">Pubblica qui il tuo annuncio</h1>
            </div>
        </div>

        

        <div class="row justify-content-center">
            <div class="col-12 col-md-10 col-lg-6 mb-5">

                <?php if($gender == 'm'): ?>

                    <img class='img-fluid' src="../img/OnlineShoppingM.svg" alt="">

                <?php elseif($gender=='other'): ?>

                    <img class='img-fluid' src="../img/OnlineShoppingO.svg" alt="">

                <?php else: ?>

                    <img class='img-fluid' src="../img/OnlineShoppingF.svg" alt="">

                <?php endif; ?>

            </div>

            <div class="col-12 col-sm-9 col-md-7 col-lg-5">

                
                <form method="POST" action="<?php echo e(route('announcements.create')); ?>" enctype="multipart/form-data">
                    <div id='form'>
                        <?php echo csrf_field(); ?>
                        <?php if($errors->any()): ?>
                            <div class="alert alert-danger">
                                <ul>
                                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><?php echo e($error); ?></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        <?php endif; ?>

                        <input type="hidden" name="uniqueSecret" value="<?php echo e($uniqueSecret); ?>">

                        <div class="row ml-2 mt-3">
                            <div class="col-12 text-center">
                                <h1>Inserisci annuncio:</h1>
                            </div>
                        </div>

                        <div class="row flex-column mx-2 mt-2">
                            <div class="col-12">
                                <div class="mt-3">
                                    <span class="span label-creazione mt-5 mb-3">Categoria</span>
                                    <select class="selectcategoria" name="category_id">
                                        <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                            <option value="<?php echo e($category->id); ?>"><?php echo e($category->name); ?></option>

                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                                <div class="mt-4">
                                    <label class="label-creazione mt-3" for="nome">Titolo annuncio</label>
                                    <input id="inputcreazione" type="text" name="title" value="<?php echo e(old('title')); ?>" />
                                </div>

                                <div class="mt-3">
                                    <label class="label-creazione mt-4" for="price">Prezzo</label>
                                    <input id="inputcreazione" type="text" name="price" value="<?php echo e(old('price')); ?>" />
                                </div>

                                <div class="mt-3">
                                    <label class="label-creazione mt-4" for="messaggio">Descrizione</label>
                                    <textarea name="description" cols="30" rows="10"><?php echo e(old('description')); ?></textarea>
                                </div>

                                <div class="mt-3">
                                    <label class="label-creazione mt-4" for="images">Immagini</label>
                                    
                                    <div class="dropzone rounded" id="drophere"></div>

                                    <?php $__errorArgs = ['images'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                        <span class="invalid-feedback" role="alert">
                                            <strong><?php echo e($message); ?></strong>
                                        </span>

                                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-12 d-flex justify-content-center">
                                <button width="600px" type="submit" class="btn btn-orange mt-5 mb-3 py-2 px-5">Pubblica</button>
                            </div>
                        </div>

                    </div>
                </form>

            </div>

        </div>

    </div>

 <?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?>
<?php /**PATH C:\Users\Roberto\wa\progetti\presto\resources\views/announcements/new.blade.php ENDPATH**/ ?>