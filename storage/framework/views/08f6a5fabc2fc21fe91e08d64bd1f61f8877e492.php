<nav class="navbar navbar-expand-lg navbar-custom">
    <a class="navbar-brand" href="<?php echo e(route('home')); ?>"><i class="fab fa-pinterest-p fa-2x navbar-icon"></i></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon navbar-icon"><i class="fas fa-bars"></i></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            

            <li class="nav-item active mx-2">
                <a class="nav-link btn-orange py-2 px-3" href="<?php echo e(route('announcements.new')); ?>"><?php echo e(__('ui.new.announcement')); ?></a>
            </li>
            <li class="nav-item dropdown mx-2">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    <?php echo e(__('ui.navbar-category')); ?>

                </a>
                <div class="dropdown-menu border-0" aria-labelledby="navbarDropdown">
                    <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        <a class="nav-link dropdown-item"
                            href="<?php echo e(route('public.announcementsByCategory', [$category->name, $category->id])); ?>"><?php echo e($category->name); ?></a>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </li>

            
            
        </ul>
  
        <ul class="navbar-nav ml-auto">

            <!-- Authentication Links -->
            <?php if(auth()->guard()->guest()): ?>
                <?php if(Route::has('login')): ?>
                    <li class="nav-item mx-1">
                        <a class="nav-link" href="<?php echo e(route('login')); ?>"><?php echo e(__('Login')); ?></a>
                    </li>
                <?php endif; ?>

                <?php if(Route::has('register')): ?>
                    <li class="nav-item">
                        <a class="nav-link mx-1" href="<?php echo e(route('register')); ?>"><?php echo e(__('Register')); ?></a>
                    </li>
                <?php endif; ?>
            <?php else: ?>
                <?php if(Auth::user()->is_revisor): ?>
                    <li class="nav-item">
                        <a href="<?php echo e(route('revisor.home')); ?>" class="nav-link mr-1"><i
                                class="fas fa-clipboard-check mr-2"></i>
                            <span
                                class="badge badge-pill badge-warning"><?php echo e(\App\Models\Announcement::toBeRevisionedCount()); ?>

                            </span>
                        </a>
                    </li>

                    <li class="nav-item mr-3">
                        <a href="<?php echo e(route('revisor.trash')); ?>" class="nav-link mr-1"><i class="far fa-trash-alt mr-2"></i>
                            <span class="badge badge-pill badge-danger"><?php echo e(\App\Models\Announcement::trashCount()); ?>

                            </span></a>
                    </li>
                <?php endif; ?>
              
                <li class="nav-item dropdown mx-1">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false" v-pre>
                        <?php echo e(__('ui.hi')); ?> <?php echo e(Auth::user()->name); ?>

                        
                    </a>

                    <div class="dropdown-menu border-0 dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item nav-link text-center" href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            <?php echo e(__('Logout')); ?>

                        </a>
                        <a class="dropdown-item nav-link text-center" href="<?php echo e(route('announcements.userpage')); ?>">Pagina utente</a>

                        <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" class="d-none">
                            <?php echo csrf_field(); ?>
                        </form>
                    </div>
                </li>
            <?php endif; ?>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                   <?php echo e(__("ui.language")); ?>

                </a>
                <div class="dropdown-menu border-0" aria-labelledby="navbarDropdown">
                    
                    <form action="<?php echo e(route('locale', 'it')); ?>" method="POST">
                    <?php echo csrf_field(); ?>
                        <button type="submit" class="nav-link dropdown-item" style="background: transparent ; border: none">
                            <span class="flag-icon flag-icon-it mr-2"></span> IT
                        </button> 
                    </form>
                    <form action="<?php echo e(route('locale', 'en')); ?>" method="POST">
                    <?php echo csrf_field(); ?>
                        <button type="submit" class="nav-link dropdown-item" style="background: transparent ; border: none">
                            <span class="flag-icon flag-icon-gb mr-2"></span> ENG
                        </button> 
                    </form>
                    <form action="<?php echo e(route('locale', 'es')); ?>" method="POST">
                    <?php echo csrf_field(); ?>
                        <button type="submit" class="nav-link dropdown-item" style="background: transparent ; border: none">
                            <span class="flag-icon flag-icon-es mr-2"></span> ES
                        </button> 
                    </form>

                </div>
            </li>
        </ul>
    </div>

</nav>
<?php /**PATH C:\Users\Roberto\wa\progetti\presto\resources\views/components/navbar.blade.php ENDPATH**/ ?>