


<footer id="sticky-footer" class="mt-5 pt-4 pb-2 text-footer">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 d-flex justify-content-center">
                <?php if(auth()->guard()->guest()): ?>
                    
                <?php else: ?>
        
                <?php if(Auth::user()->is_revisor): ?>
        
                    <p><?php echo e(__('ui.hi-revisor')); ?></p>
        
                <?php elseif(Auth::user()): ?>
        
                    <a href="<?php echo e(route('revisor.register')); ?>" class="revisor"><?php echo e(__('ui.become-revisor')); ?></a>
        
                <?php endif; ?>
        
                <?php endif; ?>
            </div>
    
            <?php if(auth()->guard()->guest()): ?>
            <div class="col-12 d-flex justify-content-center">
                <p>Copyright &copy; Roberto Mosso</p>
            </div>
            <?php else: ?>
        
                <?php if(Auth::user()->is_revisor): ?>
        
                    <div class="col-12 col-md-6 d-flex justify-content-center">
                <p>Copyright &copy; Roberto Mosso</p>
            </div>
        
                <?php elseif(Auth::user()): ?>
        
                    <div class="col-12 col-md-6 d-flex justify-content-center">
                <p>Copyright &copy; Roberto Mosso</p>
            </div>
        
                <?php endif; ?>
            <?php endif; ?>

        </div>
    </div>
</footer>
<?php /**PATH C:\Users\Roberto\wa\progetti\presto\resources\views/components/footer.blade.php ENDPATH**/ ?>