// const Dropzone = require("dropzone");
const { defaultsDeep } = require("lodash");


document.addEventListener('DOMContentLoaded', ()=> {


    if ($("#drophere").length>0){
    
        let csrfToken = $('meta[name="csrf-token"]').attr('content');
        let uniqueSecret = $('input[name="uniqueSecret"]').attr('value');
    

        let myDropzone = $("#drophere").dropzone({

            
            url:"/announcements/images/upload",
            params: {
                _token: csrfToken,
                uniqueSecret: uniqueSecret
            },
            
            addRemoveLinks: true,

            init : function(){

                let dropzone = this

                $.ajax({
                    type: 'GET',
                    url: '/announcements/images',
                    data: {
                        uniqueSecret:uniqueSecret
                    },
                    dataType: 'json'
                }).done(function(data){
                    $.each(data,function(key,value){
                        let file={
                            serverId:value.id
                        };
                        dropzone.options.addedfile.call(dropzone,file);
                        dropzone.options.thumbnail.call(dropzone, file,value.src);
                    });                  
                });

                this.on("success", function (file, response) {
                    file.serverId = response.id;
                });

                this.on("removedfile", function (file) {
                    $.ajax({
                        type: 'DELETE',
                        url: '/announcements/images/remove',
                        data: {
                            _token: csrfToken,
                            id: file.serverId,
                            uniqueSecret: uniqueSecret

                        },
                        dataType: 'json'
                    });
                });

            }
    
        });
        
    }
})

// $(function(){

   


        // let myDropzone = new Dropzone('#drophere', {
         //     url: "/announcement/images/upload",

         //     params: {
         //         _token: csrfToken,
         //         uniqueSecret: uniqueSecret
         //     }
         // });
//     }
// })

