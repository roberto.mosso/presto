<x-layout>

    <header class="masthead img-fluid">
        <div class="container h-100">
            <div class="row justify-content-center">
                <div class="col-6">
                    @if (session('success'))
                        <div class="alert alert-success text-center mt-2">
                            {{ session('success') }}
                        </div>
                    @endif
                    @if (session('access.denied.revisor.only'))
                        <div class="alert alert-danger text-center mt-2">
                            Accesso non consentito - solo per revisori
                        </div>
                    @endif
                </div>
            </div>
            <div class="row h-100 align-items-center">
                <div id="header-index"
                    class="col-12 col-md-6 text-center bg-filter d-flex flex-column justify-content-center align-items-center offset-md-6 p-2">
                    <h1 class="font-weight-bold title-custom h1-header mt-2">Presto</h1>
                    <p class="h4 text-header p-header">{{ __('ui.welcome') }}</p>

                    <form action="{{ route('announcements.searchResults') }}" method="GET">
                        <div class="wrap mt-3 mb-4">
                            <div class="search w-auto">
                                <input type="text" name="q" class="searchTerm" placeholder="{{ __('ui.search') }}">
                                <button type="submit" class="searchButton">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </header>


    <div class="container mt-5">
        <div class="row text-center">
            <div class="col-12 my-4">
                <h1 class="text-center h1-view">{{ __('ui.last-announcements') }}</h1>
            </div>
        </div>
        <div class="row">

            @foreach ($announcements as $announcement)

                <div class="col-12 col-md-6 col-lg-4 d-flex justify-content-center my-4">
                    <div class="card mt-4" style="width: 18rem;">

                        @if ($announcement->images->first() == null)

                            <img src="https://via.placeholder.com/320x320" class="card-img-top" alt="salve">

                        @else

                            <img src="{{ $announcement->images->first()->getUrl(320, 320) }}" class="card-img-top"
                                alt="ciao">

                        @endif

                        <div class="card-body">
                            <p class="text-center">{{ __('ui.category') }}:
                                <a
                                    href="{{ route('public.announcementsByCategory', [$announcement->category->name, $announcement->category->id]) }}">{{ $announcement->category->name }}</a>
                            </p>

                            <h4 class="card-title my-4">{{ $announcement->title }}</h4>

                            <p class="card-text">{{ __('ui.price') }}: {{ $announcement->price }} €</p>

                            <p class="card-text">{{ $announcement->troncate($announcement->description) }}</p>

                            <p class="card-text">{{ __('ui.author-data') }}: {{ $announcement->user->name }},
                                {{ $announcement->created_at->format('d-m-Y') }}</p>
                            <div class="text-center">
                                <a href="{{ route('announcements.detail', $announcement) }}"
                                    class="btn btn-orange my-3">{{ __('ui.go-announcement') }}</a>
                            </div>

                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>


</x-layout>
