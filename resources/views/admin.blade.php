<x-layout>


    <div class="container mt-5 vh-100">
        <div class="row">
            <div class="col-12">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nome</th>
                            {{-- <th scope="col">Email</th> --}}
                            <th scope="col">Status</th>
                            <th scope="col" class="text-center">Switch</th>
                        </tr>
                    </thead>

                    @foreach ($users as $user)


                    <tbody>

                        <tr>

                            <th scope="row">{{ $user->id }}</th>
                            <td>{{ $user->name }}</td>
                            {{-- <td>{{ $user->email }}</td> --}}

                                @if($user->is_revisor === 0)
                            
                                <td><i class="fas fa-times fa-2x text-danger ml-2"></i></td>
                            
                                @else
                            
                                <td><i class="fas fa-check fa-2x text-success ml-2"></i></td>

                                @endif
                
                            <td class="d-flex justify-content-center">
                                <form action="{{route('admin.switch', $user)}}" method="POST">
                                    @csrf
                                    <button class="btn btn-orange " type="submit">Cambia</button>
                                </form>
                            </td>

                        </tr>
        
                    </tbody>

                    @endforeach

                </table>
            </div>
        </div>
    </div>


</x-layout>