
<x-layout>


    <div class="container-fluid" id="body">

        <div class="row">
            <div class="col-12">
                <h1 class="titolo-form text-center text-blue mt-5 mb-3 h1-view">Pubblica qui il tuo annuncio</h1>
            </div>
        </div>

        {{-- <h3 class="text-dark">DEBUG:: SECRET {{ $uniqueSecret }}</h3> --}}

        <div class="row justify-content-center">
            
            <div class="col-12 col-sm-9 col-md-7 col-lg-5">

                <form method="POST" action="{{ route('announcement.update',$announcement) }}" enctype="multipart/form-data">
                    <div id='form'>
                        @method('PUT')
                        @csrf
                       
              
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        

                        <div class="row ml-2 mt-3">
                            <div class="col-12 text-center">
                                <h1>Inserisci annuncio:</h1>
                            </div>
                        </div>

                        <div class="row flex-column mx-2 mt-2">
                            <div class="col-12">
                                <div class="mt-3">
                                    <span class="span label-creazione mt-5 mb-3">Categoria</span>
                                    <select class="selectcategoria" name="category_id">
                                        @foreach ($categories as $category)

                                            <option value="{{ $category->id }}">{{ $category->name }}</option>

                                        @endforeach
                                    </select>
                                </div>
                                <div class="mt-4">
                                    
                                    <label class="label-creazione mt-3" for="nome">Titolo annuncio</label>
                                    <input id="inputcreazione" type="text" name="title" value="{{ $announcement->title }}" />
                                </div>

                                <div class="mt-3">
                                    <label class="label-creazione mt-4" for="price">Prezzo</label>
                                    <input id="inputcreazione" type="text" name="price" value="{{ $announcement->price }}" />
                                </div>

                                <div class="mt-3">
                                    <label class="label-creazione mt-4" for="messaggio">Descrizione</label>
                                    <textarea name="description" cols="30" rows="10">{{ $announcement->description }}</textarea>
                                </div>

                                <div class="mt-3">
                                    <label class="label-creazione mt-4" for="images">Immagini</label>
                                    
                                    <div class="dropzone rounded" id="drophere"></div>

                                    @error('images')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>

                                    @enderror

                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-12 d-flex justify-content-center">
                                <button width="600px" type="submit" class="btn btn-orange mt-5 mb-3 py-2 px-5">Pubblica</button>
                            </div>
                        </div>

                    </div>
                </form>

            </div>

        </div>

    </div>

</x-layout>
