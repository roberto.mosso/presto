<x-layout>
    <div class="container">
        <div class="card my-5">
            <div class="row">
                <div class="col-md-5 my-auto">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            @foreach ($announcement->images as $image)
                                @if ($loop->first)
                                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                @else
                                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                @endif
                            @endforeach
                            {{-- <li data-target="#carouselExampleIndicators" data-slide-to="2"></li> --}}
                        </ol>
                        
                        <div class="carousel-inner">
                            
                            @foreach ($announcement->images as $image)
                                @if ($loop->first)
                                <div class="carousel-item active">
                                    <img src="{{$image->getUrl(320,320)}}" class="d-block w-100"  alt=""> 
                                </div>
                                @else
                                <div class="carousel-item">
                                    <img src="{{$image->getUrl(320,320)}}" class="d-block w-100"  alt=""> 
                                </div>
                                @endif
                                    
                            @endforeach
                           
                        </div>

                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                            data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>

                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                            data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>


                <div class="col-md-7">
                    <div class="card-body">
                        <p class="my-4">Categorie: <a
                                href="{{ route('public.announcementsByCategory', [$announcement->category->name, $announcement->category->id]) }}">{{ $announcement->category->name }}</a>
                        </p>
                        <h4 class="card-title">{{ $announcement->title }}</h4>
                        <p class="card-text my-4">Prezzo: {{ $announcement->price }} €</p>
                        <p class="card-text my-4">{{ $announcement->description }}</p>

                        <a href="{{ route('home') }}" class="btn btn-orange mt-3">Torna indietro</a>
                    </div>
                </div>


            </div>

            <div class="card-footer">
                <div class="col-12 col-md-6">
                    <span class="text-muted">Pubblicato da: {{ $announcement->user->name }}</span>
                </div>
                <div class="col-12 col-md-6">
                    <span class="text-muted">In data: {{ $announcement->created_at->format('d-m-Y') }}</span>
                </div>

            </div>
        </div>

    </div>


</x-layout>
