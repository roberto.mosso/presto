<x-layout>

    <div class="container">

        <div class="row my-5">
            <div class="col-12 text-center">
                <h1>Risultati della ricerca per: {{ $q }}</h1>
            </div>
        </div>

        <div class="row my-5">

            @foreach ($announcements as $announcement)

                <div class="col-12 col-md-6 col-lg-4 d-flex justify-content-center my-4">
                    <div class="card mt-4" style="width: 18rem;">

                        @if ($announcement->images->first() == null)

                            <img src="https://via.placeholder.com/320x320" class="card-img-top" alt="salve">

                        @else

                            <img src="{{ $announcement->images->first()->getUrl(320, 320) }}" class="card-img-top"
                                alt="ciao">

                        @endif

                        <div class="card-body">
                            <p class="text-center">{{ __('ui.category') }}:
                                <a
                                    href="{{ route('public.announcementsByCategory', [$announcement->category->name, $announcement->category->id]) }}">{{ $announcement->category->name }}</a>
                            </p>

                            <h4 class="card-title my-4">{{ $announcement->title }}</h4>

                            <p class="card-text">{{ __('ui.price') }}: {{ $announcement->price }} €</p>

                            <p class="card-text">{{ $announcement->troncate($announcement->description) }}</p>

                            <p class="card-text">{{ __('ui.author-data') }}: {{ $announcement->user->name }},
                                {{ $announcement->created_at->format('d-m-Y') }}</p>
                            <div class="text-center">
                                <a href="{{ route('announcements.detail', $announcement) }}"
                                    class="btn btn-orange my-3">{{ __('ui.go-announcement') }}</a>
                            </div>

                        </div>


                    </div>
                </div>
            @endforeach

        </div>

    </div>






</x-layout>
