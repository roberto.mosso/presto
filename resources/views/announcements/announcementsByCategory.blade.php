<x-layout>

    <div class="container mt-5">
        <div class="row my-5">
            <div class="col-12 text-center">
                <h1 class="text-center h1-view">{{ $category->name }}</h1>
            </div>
        </div>
        <div class="row my-3">
            @foreach ($announcements as $announcement)

                <div class="col-12 col-md-6 col-lg-4 d-flex justify-content-center my-4">
                    <div class="card" style="width: 18rem;">

                        @if($announcement->images->first() == null)

                            <img src="https://via.placeholder.com/320x320" class="card-img-top" alt="salve">
                        
                        @else 

                            <img src="{{$announcement->images->first()->getUrl(320,320)}}" class="card-img-top" alt="ciao">

                        @endif

                        <div class="card-body">
                            
                            <h4 class="card-title my-4">{{$announcement->title}}</h4>

                            <p class="card-text">Prezzo: {{$announcement->price}} €</p>

                            <p class="card-text">{{$announcement->troncate($announcement->description)}}</p>

                            <p class="card-text">Autore/data: {{ $announcement->user->name }},
                                {{ $announcement->created_at->format('d-m-Y') }}</p>
                            <div class="text-center">
                                <a href="{{route('announcements.detail', $announcement)}}"
                                    class="btn btn-orange my-3">Vai
                                    all'annuncio</a>
                            </div>

                        </div>
                    </div>
                </div>

            @endforeach
        </div>
        <div class="row justify-content-center mt-3">
            <div class="col-md-8">
                {{$announcements->links()}}
            </div>
        </div>
    </div>

</x-layout>
