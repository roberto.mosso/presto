<x-layout>


    <div class="container mt-5">
        <div class="row text-center">
            <div class="col-12 my-4">
                <h1 class="text-center h1-view">Iscrizione revisore</h1>
            </div>
        </div>
    </div>


    <div class="container" id="body">

        <div class="row justify-content-center">

            <div class="col-8">


                <form method="POST" action="{{ route('revisor.new') }}" enctype="multipart/form-data">
                    <div id='form'>
                        @csrf
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="container">
                            <div class="row justify-content-center">

                                <h1>Inserisci i tuoi dati</h1>

                            </div>
                        </div>

                        <div class="row">
                          <div class="col-10 offset-1">

                            <div class="mt-3">
                              <label class="label-creazione2" for="nome">Nome</label>
                              <input id="inputcreazione" type="text" name="name" value="{{ old('name') }}"/>
                            </div>

                            <div class="my-5">
                              <label class="label-creazione2" for="price">Cognome</label>
                              <input id="inputcreazione" type="text" name="surname" value="{{ old('surname') }}"/>
                            </div>

                            <div class="mt-5">
                              <label class="label-creazione2" for="price">Email</label>
                              <input id="inputcreazione" type="email" name="email" value="{{ old('email') }}"/>
                            </div>

                            <div class="my-5">
                              <label class="label-creazione2" for="messaggio">Presentati</label>
                              <textarea name="about_you" cols="30" rows="10">{{old('about_you')}}</textarea>
                            </div>

                            <div class="my-5">
                              <label class="label-creazione2" for="cv">Invia il tuo CV</label>
                              <input id="inputcreazione" type="file" name="cv"/>
                            </div>
                          </div>
                        </div>

                        <div class="row d-flex justify-content-center col-12 mt-4 mb-3">
                            {{-- <div class="col-12"> --}}

                            <button width="600px" id="btn_orange" type="submit" class="btn btn-orange py-2 px-5">Invia</button>

                            {{-- </div> --}}
                        </div>

                </form>
            </div>

        </div>
    </div>

</x-layout>
