<x-layout>

    <div class="container mt-5">
        <div class="row text-center">
            <div class="col-12 my-4">
                <h1 class="text-center h1-view">Annunci da revisionare:</h1>
            </div>
        </div>
    </div>


    @if ($announcements)
        @foreach ($announcements as $announcement)

            <div class="container my-5">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                Annuncio # {{ $announcement->id }}
                            </div>
                            <div class="card-body">
                                <div class="row my-3">
                                    <div class="col-md-3">
                                        <h3>Utente</h3>
                                    </div>
                                    <div class="col-md-9">
                                        # {{ $announcement->user->id }},
                                        {{ $announcement->user->name }},
                                        {{ $announcement->user->email }}
                                    </div>
                                </div>
                                <div class="row my-3">
                                    <div class="col-md-3">
                                        <h3>Titolo</h3>
                                    </div>
                                    <div class="col-md-9">
                                        {{ $announcement->title }}
                                    </div>
                                </div>
                                <div class="row my-3">
                                    <div class="col-md-3">
                                        <h3>Prezzo</h3>
                                    </div>
                                    <div class="col-md-9">
                                        {{ $announcement->price }} €
                                    </div>
                                </div>
                                <div class="row my-3">
                                    <div class="col-md-3">
                                        <h3>Descrizione</h3>
                                    </div>
                                    <div class="col-md-9">
                                        {{ $announcement->description }}
                                    </div>
                                </div>
                                <div class="row mt-5">
                                    <div class="col-md-3">
                                        <h3>Immagini</h3>
                                    </div>
                                    <div class="col-md-9">

                                        @foreach ($announcement->images as $image)

                                            {{-- <div class="row">

                                            <div class="col-12 mb-4">
                                                <img src="{{$image->getUrl(320,320)}}" class="rounded" alt="">
                                            </div>
                                            
                                            <div class="col-md-5 mb-4">
                                                <img src="{{$image->getUrl(320,320)}}" class="rounded" alt="">
                                            </div>
                                            <div class="col-md-3 mb-4">
                                                Adult: {{$image->adult}} <br>
                                                Spoof: {{$image->spoof}} <br>
                                                Medical: {{$image->medical}} <br>
                                                Violence: {{$image->violence}} <br>
                                                Racy: {{$image->racy}} <br>
                                            </div>
                                            <div class="col-md-3 mb-4">
                                                <h5>Labels</h5>
                                                <ul>
                                                   @if ($image->labels)
                                                    @foreach ($image->labels as $label)
                                                        <li>{{$label}}</li>
                                                    @endforeach
                                                   @endif
                                                </ul>

                                            </div>
                                        </div> --}}

                                            <div class="row">

                                                <div class="col-md-5 mb-4">
                                                    <img src="{{ $image->getUrl(320, 320) }}" class="rounded" alt="">
                                                </div>
                                                <div class="col-md-3 mb-4">
                                                    Adult: {{ $image->adult }} <br>
                                                    Spoof: {{ $image->spoof }} <br>
                                                    Medical: {{ $image->medical }} <br>
                                                    Violence: {{ $image->violence }} <br>
                                                    Racy: {{ $image->racy }} <br>
                                                </div>
                                                <div class="col-md-3 mb-4">
                                                    {{-- <h5>Labels</h5> --}}
                                                    <ul>
                                                        @if ($image->labels)
                                                            @foreach ($image->labels as $label)
                                                                <li>{{ $label }}</li>
                                                            @endforeach
                                                        @endif
                                                    </ul>

                                                </div>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <div class="row my-3">
                        <div class="col-md-6 text-center">
                            <form action="{{ route('revisor.accept', $announcement->id) }}" method="POST">
                                @csrf
                                <button type="submit" class="btn btn-success py-2 px-5">Accetta</button>
                            </form>
                        </div>
                        <div class="col-md-6 text-center">
                            <form action="{{ route('revisor.reject', $announcement->id) }}" method="POST">
                                @csrf
                                <button type="submit" class="btn btn-danger py-2 px-5">Sposta nel cestino</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @else
    @endif

    @if (\App\Models\Announcement::toBeRevisionedCount() == 0)
        <div class="container mt-5 vh-100">
            <div class="row text-center">
                <div class="col-12 my-4">
                    <h2 class="text-center">Non ci sono annunci da revisionare</h2>
                </div>
            </div>
        </div>
    @endif


    <div class="row justify-content-center mt-3">
        <div class="col-md-8">
            {{ $announcements->links() }}
        </div>
    </div>

</x-layout>
