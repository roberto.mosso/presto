<nav class="navbar navbar-expand-lg navbar-custom">
    <a class="navbar-brand" href="{{ route('home') }}"><i class="fab fa-pinterest-p fa-2x navbar-icon"></i></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon navbar-icon"><i class="fas fa-bars"></i></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            {{-- <li class="nav-item active">
          <a class="nav-link" href="#">Categorie <span class="sr-only">(current)</span></a>
        </li> --}}

            <li class="nav-item active mx-2">
                <a class="nav-link btn-orange py-2 px-3" href="{{ route('announcements.new') }}">{{__('ui.new.announcement')}}</a>
            </li>
            <li class="nav-item dropdown mx-2">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    {{__('ui.navbar-category')}}
                </a>
                <div class="dropdown-menu border-0" aria-labelledby="navbarDropdown">
                    @foreach ($categories as $category)

                        <a class="nav-link dropdown-item"
                            href="{{ route('public.announcementsByCategory', [$category->name, $category->id]) }}">{{ $category->name }}</a>

                    @endforeach
                </div>
            </li>

            {{-- <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                   {{__("ui.language")}}
                </a>
                <div class="dropdown-menu border-0" aria-labelledby="navbarDropdown">
                    
                    <form action="{{route('locale', 'it')}}" method="POST">
                    @csrf
                        <button type="submit" class="nav-link dropdown-item" style="background: transparent ; border: none">
                            <span class="flag-icon flag-icon-it"></span> IT
                        </button> 
                    </form>
                    <form action="{{route('locale', 'en')}}" method="POST">
                    @csrf
                        <button type="submit" class="nav-link dropdown-item" style="background: transparent ; border: none">
                            <span class="flag-icon flag-icon-gb"></span> ENG
                        </button> 
                    </form>
                    <form action="{{route('locale', 'es')}}" method="POST">
                    @csrf
                        <button type="submit" class="nav-link dropdown-item" style="background: transparent ; border: none">
                            <span class="flag-icon flag-icon-es"></span> ES
                        </button> 
                    </form>

                </div>
            </li> --}}
            
        </ul>
  
        <ul class="navbar-nav ml-auto">

            <!-- Authentication Links -->
            @guest
                @if (Route::has('login'))
                    <li class="nav-item mx-1">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                @endif

                @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link mx-1" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                @endif
            @else
                @if (Auth::user()->is_revisor)
                    <li class="nav-item">
                        <a href="{{ route('revisor.home') }}" class="nav-link mr-1"><i
                                class="fas fa-clipboard-check mr-2"></i>
                            <span
                                class="badge badge-pill badge-warning">{{ \App\Models\Announcement::toBeRevisionedCount() }}
                            </span>
                        </a>
                    </li>

                    <li class="nav-item mr-3">
                        <a href="{{ route('revisor.trash') }}" class="nav-link mr-1"><i class="far fa-trash-alt mr-2"></i>
                            <span class="badge badge-pill badge-danger">{{ \App\Models\Announcement::trashCount() }}
                            </span></a>
                    </li>
                @endif
              
                <li class="nav-item dropdown mx-1">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false" v-pre>
                        {{__('ui.hi')}} {{ Auth::user()->name }}
                        {{-- <img src="{{Storage::url(Auth::user()->userImg)}}" alt="" width="50"> --}}
                    </a>

                    <div class="dropdown-menu border-0 dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item nav-link text-center" href="{{ route('logout') }}" onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                        <a class="dropdown-item nav-link text-center" href="{{ route('announcements.userpage') }}">Pagina utente</a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </li>
            @endguest

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                   {{__("ui.language")}}
                </a>
                <div class="dropdown-menu border-0" aria-labelledby="navbarDropdown">
                    
                    <form action="{{route('locale', 'it')}}" method="POST">
                    @csrf
                        <button type="submit" class="nav-link dropdown-item" style="background: transparent ; border: none">
                            <span class="flag-icon flag-icon-it mr-2"></span> IT
                        </button> 
                    </form>
                    <form action="{{route('locale', 'en')}}" method="POST">
                    @csrf
                        <button type="submit" class="nav-link dropdown-item" style="background: transparent ; border: none">
                            <span class="flag-icon flag-icon-gb mr-2"></span> ENG
                        </button> 
                    </form>
                    <form action="{{route('locale', 'es')}}" method="POST">
                    @csrf
                        <button type="submit" class="nav-link dropdown-item" style="background: transparent ; border: none">
                            <span class="flag-icon flag-icon-es mr-2"></span> ES
                        </button> 
                    </form>

                </div>
            </li>
        </ul>
    </div>

</nav>
