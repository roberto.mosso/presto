


<footer id="sticky-footer" class="mt-5 pt-4 pb-2 text-footer">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 d-flex justify-content-center">
                @guest
                    
                @else
        
                @if (Auth::user()->is_revisor)
        
                    <p>{{__('ui.hi-revisor')}}</p>
        
                @elseif (Auth::user())
        
                    <a href="{{route('revisor.register')}}" class="revisor">{{__('ui.become-revisor')}}</a>
        
                @endif
        
                @endguest
            </div>
    
            @guest
            <div class="col-12 d-flex justify-content-center">
                <p>Copyright &copy; Roberto Mosso</p>
            </div>
            @else
        
                @if (Auth::user()->is_revisor)
        
                    <div class="col-12 col-md-6 d-flex justify-content-center">
                <p>Copyright &copy; Roberto Mosso</p>
            </div>
        
                @elseif (Auth::user())
        
                    <div class="col-12 col-md-6 d-flex justify-content-center">
                <p>Copyright &copy; Roberto Mosso</p>
            </div>
        
                @endif
            @endguest

        </div>
    </div>
</footer>
