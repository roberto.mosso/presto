<?php

return [

    'welcome' => 'Compra con un clic!',
    'last-announcements' => 'Últimos 5 anuncios cargados:',
    'category' => 'Categoría',
    'new.announcement' => 'Nuevo anuncio',
    'hi' => 'Hola',
    'search' => '¿Qué busca?',
    'price' => 'Precio',
    'author-data' => 'Autor/fecha',
    "go-announcement" => "Ir al anuncio",
    'navbar-category' => 'Categorias',
    'hi-revisor' => 'Buen revisor de trabajo',
    'become-revisor' => '¿Quieres convertirte en auditor? haga clic aquí',
    'language' => 'Lengua',


];
