<?php

return [

    'welcome' => 'Buy with a click!',
    'last-announcements' => 'The last 5 ads loaded:',
    'category' => 'Category',
    'new.announcement' => 'New announcement',
    'hi' => 'Hi',
    'search' => 'What are you looking for?',
    'price' => 'Price',
    'author-data' => 'Author/date',
    "go-announcement" => "Go to announcement",
    'navbar-category' => 'Category',
    'hi-revisor' => 'Good job reviewer',
    'become-revisor' => 'Do you want to become an auditor? Click here',
    'language' => 'Language',

  
];
