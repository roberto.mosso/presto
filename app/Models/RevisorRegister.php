<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RevisorRegister extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'surname',
        'email',
        'about_you',
        'cv'
    ];
}
