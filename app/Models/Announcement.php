<?php

namespace App\Models;

use App\Models\User;
use App\Models\Category;
use Laravel\Scout\Searchable;
use App\Models\AnnouncementImage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Announcement extends Model
{
    use HasFactory;

    use Searchable;
   
    protected $fillable = [
        'title',
        'description',
        'category_id',
        'user_id',
        'price'
    ];

    public function toSearchableArray()
    {
        $array = [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
        ];
        
        return $array;
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }


    public function troncate($description){
        $description=strip_tags($description);
        $description=substr($description,0,75);
        $description=$description."...";
        return $description;
     }


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    
    public function images()
    {
        return $this->hasMany(AnnouncementImage::class);
    }

     
    static public function toBeRevisionedCount()
    {
        return Announcement::where('is_accepted',null)->count();
    }

    
    static public function trashCount()
    {
        return Announcement::where('is_accepted', false)->count();
    }
}
