<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RevisorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|between: 3, 24',
            'surname'=>'required|between: 3, 24',
            'email'=>'required|email:rfc,dns',
            'about_you'=>'required|between: 20, 240',
            'cv'=>'required|file',
        ];
    }
}
