<?php

namespace App\Http\Controllers;

use App\Models\Announcement;
use App\Models\AnnouncementImage;
use Illuminate\Http\Request;

class RevisorController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth.revisor');
    }


    public function index(){
        $announcements=Announcement::where('is_accepted',null)->orderBy('created_at')->paginate(5);
        return view("revisor.home",compact('announcements'));
    }


    private function setAccepted($announcement_id,$value){
        $announcement = Announcement::find($announcement_id);
        $announcement-> is_accepted = $value;
        $announcement->save();
        return redirect(route('revisor.home'));
    }


    public function accept($announcement_id){
        return $this->setAccepted($announcement_id,true);
    }
    

    public function reject($announcement_id){
        return $this->setAccepted($announcement_id,false);
    }


    public function trash()
    {
        // $announcements = Announcement::all();
        $announcements = Announcement::where('is_accepted', false)->orderBy('created_at')->paginate(5);
        return view("revisor.trash", compact('announcements'));
    }


    private function accettato($announcement_id, $value)
    {
        $announcement = Announcement::find($announcement_id);
        $announcement->is_accepted = $value;
        $announcement->save();
        return redirect(route('revisor.trash'));
    }


    public function accetta($announcement_id)
    {
        return $this->accettato($announcement_id, true);
    }


    public function destroy(Announcement $announcement)
        
    {   
        $images=AnnouncementImage::where('announcement_id',$announcement->id)->get();
        foreach($images as $image){
          $image->delete();

        }
     
        $announcement->delete();
        return redirect(route('revisor.trash')); 
    }


}
