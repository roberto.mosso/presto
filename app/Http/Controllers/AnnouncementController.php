<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Category;
use App\Jobs\ResizeImage;
use App\Jobs\ImgWatermark;
use App\Models\Announcement;
use Illuminate\Http\Request;
use App\Models\AnnouncementImage;
use Illuminate\Routing\Controller;
use App\Jobs\GoogleVisionLabelImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Jobs\GoogleVisionRemoveFaces;
use Illuminate\Support\Facades\Storage;
use App\Jobs\GoogleVisionSafeSearchImage;
use App\Http\Requests\AnnouncementRequest;

class AnnouncementController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    

    public function newannouncements(Request $request){

        $categories=Category::all();
        $gender=Auth::user()->gender;       
        $uniqueSecret =$request->old('uniqueSecret', base_convert(sha1(uniqid(mt_rand())), 16, 36));
  
        return view('announcements.new',compact('categories','gender','uniqueSecret'));
    }


    public function userpage(){
        $announcements=Announcement::all();
        $user=Auth::user();
          
        return view('announcements.userpage',compact('user','announcements'));
    }


    public function edit(Announcement $announcement){
       
       
        return view('announcements.edit',compact('announcement'));
    }


    public function update(Request $request,Announcement $announcement)
    {
        $announcement->title = $request->title;
        $announcement->price = $request->price;
        $announcement->description = $request->description;
        
        $announcement->save();
       return redirect(route('announcements.userpage',compact('announcement')));
    }


    public function submit(AnnouncementRequest $request){

        $announcement = Announcement::create([
            'title' => $request->title,
            'description' => $request->description,
            'category_id' => $request->category_id,
            'user_id'=>Auth::id(),
            'price'=>$request->price
           
        ]);

        $uniqueSecret = $request->input('uniqueSecret');

        $images=session()->get("images.{$uniqueSecret}",[]);

        $removedImages =session()->get("removedimages.{$uniqueSecret}",[]);

        $images = array_diff($images, $removedImages);

        foreach($images as $image){

            $i = new AnnouncementImage();

            $fileName = basename($image);

            $newFileName = "public/announcements/{$announcement->id}/{$fileName}";

            Storage::move($image, $newFileName);


            // dispatch(new ResizeImage(
            //     $fileName,
            //     320,
            //     320
            // ));

            $i->file=$newFileName;

            $i->announcement_id=$announcement->id;

            $i->save();



            GoogleVisionSafeSearchImage::withChain([
                new GoogleVisionLabelImage($i->id),
                new GoogleVisionRemoveFaces($i->id),
                new ResizeImage($i->file,320,320),
                // new ResizeImage($i->file,440,440)
             ])->dispatch($i->id);

        }

        File::deleteDirectory(storage_path("/app/public/temp/{$uniqueSecret}"));

        return redirect(route('home'))->with('success', 'Annuncio inviato correttamente. Ora sarà sottoposto a revisione');
    }


    public function user(){
        return $this->belongsTo(User::class);
    } 


    public function uploadImage(Request $request){

        $uniqueSecret = $request->input('uniqueSecret');

        $fileName = $request->file('file')->store("public/temp/{$uniqueSecret}");

        dispatch(new ResizeImage(
            $fileName,
            120,
            120
        ));

        session()->push("images.{$uniqueSecret}", $fileName);
        

        return response()->json(
            [
                'id' => $fileName
            ]
        );
    }


    public function removeImage(Request $request){

        $uniqueSecret = $request->input('uniqueSecret');

        $fileName = $request->input('id');

        session()->push("removedimages.{$uniqueSecret}", $fileName);

        Storage::delete($fileName);

        return response()->json('ok');
    }

    
    public function getImages(Request $request){

        $uniqueSecret = $request->input('uniqueSecret');
        $images = session()->get("images.{$uniqueSecret}",[]);
        $removedImages=session()->get("removedimages.{$uniqueSecret}", []);
        $images=array_diff($images,$removedImages);
        $data=[];
        foreach($images as $image){
            $data[]=[
                'id' => $image,
                'src' => AnnouncementImage::getUrlByFilePath($image,120,120),
                
            ];
        }
        return response()->json($data);
           
    }
}
