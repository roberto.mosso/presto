<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Mail\NewRevisor;
use App\Models\Category;
use App\Models\Announcement;
use Illuminate\Http\Request;
use Laravel\Ui\Presets\React;
use App\Models\RevisorRegister;
use Illuminate\Support\Facades\Mail;

class PublicController extends Controller
{
    public function index()
    {   
    
        $announcements=Announcement::where('is_accepted', true)
        ->orderBy('created_at','desc')->take(6)->get();
        return view('home',compact('announcements'));
    }


    public function detail(Announcement $announcement){
        
        return view('announcements.detail',compact('announcement'));
    }


    public function FilterByCategory($name, $category_id){
        
        $category=Category::find($category_id);
        $announcements=$category->announcements()->where('is_accepted',true)->orderBy('created_at','desc')->paginate(5);
        return view('announcements.announcementsByCategory',compact('category','announcements'));
    }

    public function revisorForm(){
        return view ('revisor.register');
    }

    public function search(Request $request){

        $q = $request->input('q');

        $announcements = Announcement::search($q)->get();
        
        return view('announcements.searchResults', compact('q', 'announcements'));
    }
        

    public function locale($locale)
    {
        session()->put('locale', $locale);

        return redirect()->back();
    }

    public function admin(User $users)
    {

        $users = User::all();
        // dd($users);
        return view('admin', compact('users'));

    }

    public function switch(User $users)
    {
        
        $users = User::find($users->id);
        
        // dd($users);
        // $users = User::all();
        // $users = User::where()->get();

        // foreach ($users as $user) {

            if ($users->is_revisor == 0) {
                
                $users->is_revisor = 1;

                $users->save();

            } else {

                $users->is_revisor = 0;
                
                $users->save();
            };
            
        // }
        

        return redirect()->back();
    }
}