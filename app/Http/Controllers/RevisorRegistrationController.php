<?php

namespace App\Http\Controllers;

use App\Mail\NewRevisor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\RevisorRequest;

class RevisorRegistrationController extends Controller
{
    public function submit(RevisorRequest $request)
    {
        $name=$request->input('name');
        $email=$request->input('email');
        $surname=$request->input('surname');
        $about_you=$request->input('about_you');
        $cv=$request->input('cv');
        $contact=compact('name','surname','about_you');

        Mail::to('admin@presto.it')->send(new NewRevisor($contact));
        return redirect(route('home'))->with('success', 'Abbiamo ricevuto i tuoi dati');
    }
}
