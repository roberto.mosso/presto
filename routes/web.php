<?php

use App\Models\Announcement;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\RevisorController;
use App\Http\Controllers\AnnouncementController;
use App\Http\Controllers\RevisorRegistrationController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', [PublicController::class, 'index'])->name('home');
Route::get('/search', [PublicController::class, 'search'])->name('announcements.searchResults');
Route::post('/locale/{locale}', [PublicController::class, 'locale'])->name('locale');


// announcement
Route::get('/announcements/new',[AnnouncementController::class,'newannouncements'])->name('announcements.new');
Route::post('/announcements/create',[AnnouncementController::class,'submit'])->name('announcements.create');
Route::post('/announcements/images/upload', [AnnouncementController::class, 'uploadImage'])->name('announcements.uploadImage');
Route::delete('/announcements/images/remove', [AnnouncementController::class, 'removeImage'])->name('announcements.removeImage');
Route::get('/announcements/images', [AnnouncementController::class, 'getImages'])->name('announcements.images');
Route::get('/user/page/',[AnnouncementController::class,'userpage'])->name('announcements.userpage');
Route::get('/announcements/detail/{announcement}',[PublicController::class,'detail'])->name('announcements.detail');
Route::get('/category/{name}/{id}/announcements',[PublicController::class,'FilterByCategory'])->name('public.announcementsByCategory');
Route::get('/announcement/edit/{announcement}', [AnnouncementController::class,'edit'])->name('announcement.edit');
Route::put('/announcement/update/{announcement}', [AnnouncementController::class,'update'])->name('announcement.update');
// revisore
// Route::get('/revisor/home', [RevisorController::class, 'index'])->name('home');
Route::get('/revisor/home', [RevisorController::class, 'index'])->name('revisor.home');
Route::post('/revisor/announcements/{id}/accept',[RevisorController::class,'accept'])->name('revisor.accept');
Route::post('/revisor/announcements/{id}/reject',[RevisorController::class,'reject'])->name('revisor.reject');

Route::get('/revisor/register', [PublicController::class, 'revisorForm'])->name('revisor.register');
Route::post('/revisor/register/submit', [RevisorRegistrationController::class, 'submit'])->name('revisor.new');




















Route::get('/revisor/trash', [RevisorController::class, 'trash'])->name('revisor.trash');
Route::post('/revisor/trash/{id}/accetta', [RevisorController::class, 'accetta'])->name('revisor.accetta');
Route::delete('/revisor/trash/{announcement}/destroy', [RevisorController::class, 'destroy'])->name('revisor.destroy');













Route::get('/admin', [PublicController::class, 'admin'])->name('admin');
Route::post('/admin/switch/{users}', [PublicController::class, 'switch'])->name('admin.switch');

